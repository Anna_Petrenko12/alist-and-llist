package ArrayList;

public class AList implements IList{

    static int[] array;

    public AList(){
        array= new int[10];
    }

    public void AList(int capacity){
        array= new int[capacity + (capacity/100*20)];
    }

    public void AList(int[] array){
        array = new int[array.length + (array.length / 100 * 20)];
       for(int i=0;i< array.length;i++){
           array[i]=array[i];
       }
    }

    @Override
    public int size() {
        return array.length;
    }

    @Override
    public void clear() {
        array=null;
    }



    @Override
    public int get(int index) {
        return array[index];
    }

    @Override
    public boolean add(int number) {
        for (int i =0;i<array.length;i++){
            array[i]=number;
            return true;
        }

            return false;
    }

    @Override
    public boolean add(int index, int number) {
        return false;
    }

    @Override
    public int remove(int number) {
        return 0;
    }

    @Override
    public int removeByIndex(int index) {
        return 0;
    }

    @Override
    public boolean contains(int number) {
        return false;
    }

    @Override
    public void print() {

    }

    @Override
    public int[] toArray() {
        return new int[0];
    }

    @Override
    public IList subList(int fromIdex, int toIndex) {
        return null;
    }

    @Override
    public boolean removeAll(int[] arr) {
        return false;
    }

    @Override
    public boolean retainAll(int[] arr) {
        return false;
    }
}
